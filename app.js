/**
 * This is only used when running via `npm run service-runner`.
 * When running with that command, the entrypoint is server.js
 */

const http = require("node:http");
const addShutdown = require("http-shutdown");
// Dynamic import to handle CommonJS <-> Modules mismatch
const nitroServer = () => import("./.output/server/index.mjs");
const { v1: uuidv1 } = require("uuid");

/**
 * Generates an object suitable for logging out of a request object
 *
 * @param {!Request} req          the request
 * @param {?RegExp}  whitelistRE  the RegExp used to filter headers
 * @return {!Object} an object containing the key components of the request
 */
function reqForLog(req, whitelistRE) {
  const ret = {
    url: req.originalUrl,
    headers: {},
    method: req.method,
    params: req.params,
    query: req.query,
    body: req.body,
    remoteAddress: req.connection.remoteAddress,
    remotePort: req.connection.remotePort,
  };

  if (req.headers && whitelistRE) {
    Object.keys(req.headers).forEach((hdr) => {
      if (whitelistRE.test(hdr)) {
        ret.headers[hdr] = req.headers[hdr];
      }
    });
  }

  return ret;
}

/**
 * Adds logger to the request and logs it.
 *
 * @param {http.IncomingMessage} req request object
 * @param {options} options service-runner options object
 */
function initAndLogRequest(req, options) {
  req.headers = req.headers || {};
  req.headers["x-request-id"] = req.headers["x-request-id"] || uuidv1();
  req.logger = options.logger.child({
    request_id: req.headers["x-request-id"],
    request: reqForLog(req, options.config.log_header_whitelist),
  });
  req.context = { reqId: req.headers["x-request-id"] };
  req.logger.log("trace/req", { msg: "Incoming request" });
  return req;
}

/**
 * Creates and start the service's web server
 *
 * @param {object} options the options object from service runner to use in the service
 * @return {http.Server} a web server
 */
module.exports = async (options) => {
  const { handler: nitroHandler } = await nitroServer();

  // These pasted from service-template-node.
  // Some might be unnecessary

  // ensure some sane defaults
  options.config.port = options.config.port || 8888;
  options.config.interface = options.config.interface || "0.0.0.0";
  options.config.compression_level =
    options.config.compression_level === undefined
      ? 3
      : options.config.compression_level;
  options.config.cors =
    options.config.cors === undefined ? "*" : options.config.cors;
  if (options.config.csp === undefined) {
    options.config.csp =
      "default-src 'self'; object-src 'none'; media-src *; img-src *; style-src *; frame-ancestors 'self'";
  }

  // set outgoing proxy
  if (options.config.proxy) {
    process.env.HTTP_PROXY = options.config.proxy;
    // if there is a list of domains which should
    // not be proxied, set it
    if (options.config.no_proxy_list) {
      if (Array.isArray(options.config.no_proxy_list)) {
        process.env.NO_PROXY = options.config.no_proxy_list.join(",");
      } else {
        process.env.NO_PROXY = options.config.no_proxy_list;
      }
    }
  }

  // set up the user agent header string to use for requests
  //   options.config.user_agent = options.config.user_agent || app.info.name;

  // set up header whitelisting for logging
  if (!options.config.log_header_whitelist) {
    options.config.log_header_whitelist = [
      "cache-control",
      "content-type",
      "content-length",
      "if-match",
      "user-agent",
      "x-request-id",
    ];
  }
  options.config.log_header_whitelist = new RegExp(
    `^(?:${options.config.log_header_whitelist
      .map((item) => {
        return item.trim();
      })
      .join("|")})$`,
    "i"
  );

  let server = http.createServer(async (req, res) => {
    if (options.config.cors !== false) {
      res.setHeader("access-control-allow-origin", options.config.cors);
      res.setHeader(
        "access-control-allow-headers",
        "accept, x-requested-with, content-type"
      );
      res.setHeader("access-control-expose-headers", "etag");
    }
    if (options.config.csp !== false) {
      res.setHeader("x-xss-protection", "1; mode=block");
      res.setHeader("x-content-type-options", "nosniff");
      res.setHeader("x-frame-options", "SAMEORIGIN");
      res.setHeader("content-security-policy", options.config.csp);
      res.setHeader("x-content-security-policy", options.config.csp);
      res.setHeader("x-webkit-csp", options.config.csp);
    }

    req = initAndLogRequest(req, options);

    // Attaching the entire service runner options just in case
    req.serviceRunnerOptions = options;

    return nitroHandler(req, res);
  });

  server.listen(options.config.port, options.config.interface);
  server = addShutdown(server);
  options.logger.log(
    "info",
    `Worker ${process.pid} listening on ${options.config.interface || "*"}:${
      options.config.port
    }`
  );

  return server;
};
