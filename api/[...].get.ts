import { getConfigStore } from "../utils/config";
import ServiceUtils from "@tchin/service-utils";

export default defineEventHandler(async (event) => {
  const configPath = getRequestURL(event).pathname.replace("/api/", "");
  let logger: any;

  // This logger comes from service-runner.
  // It's not specific to the event, but because of how this service
  // is currently built, the easiest way we can get the logger in Nitro
  // is by attaching it to an event.
  if (event.node.req["serviceRunnerOptions"]) {
    logger = event.node.req["serviceRunnerOptions"]["logger"];
    logger.log(
      "info",
      `Config requested: ${configPath}`
    );
  } else {
    logger = ServiceUtils.getLogger();
  }

  const { git_sha } = getQuery<{ git_sha: string }>(event);

  const configStore = getConfigStore(logger);

  try {
    return await configStore.getNormalizedConfig(configPath, {
      gitSHA: git_sha,
    });
  } catch (e) {
    if (logger) {
      logger.log("error", e);
    }
    if (e.code === "ENOENT") {
      sendNoContent(event);
    }
    sendError(event, e);
  }
});
