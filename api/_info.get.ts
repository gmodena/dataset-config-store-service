import { version } from "../package.json";

export default defineEventHandler((event) => {
  return {
    version,
  };
});
