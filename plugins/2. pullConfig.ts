import { execSync } from "child_process";
import ServiceUtils from "@tchin/service-utils";

export default defineNitroPlugin(async (nitroApp) => {
  // Plugins in Nitro (currently) load async, so we initialize here instead of serviceUtils so we can log
  await ServiceUtils.initialize();
  const logger = ServiceUtils.getLogger();
  logger.log(
    "info",
    `Using Config Store Backend: ${useRuntimeConfig().configStore?.backend}`
  );
  if (
    useRuntimeConfig().configStore?.backend !== "repo" ||
    !useRuntimeConfig().configStore?.pullRepo
  ) {
    return;
  }
  const configRepoUrl = useRuntimeConfig().configStore?.configRepoProjectPath;
  if (!configRepoUrl) {
    return;
  }
  const gitUrl = `https://gitlab.wikimedia.org/${configRepoUrl}.git`;
  execSync(`rm -rf config && git clone ${gitUrl} config`, {
    stdio: [0, 1, 2],
    cwd: useRuntimeConfig().configStore?.gitDirectory ?? process.env.PWD,
  });
  logger.log("info", "Finished cloning config store");
});
