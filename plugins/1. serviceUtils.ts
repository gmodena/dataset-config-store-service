import ServiceUtils from "@tchin/service-utils";

export default defineNitroPlugin(async (nitroApp) => {
  // await ServiceUtils.initialize();

  nitroApp.hooks.hook("close", async () => {
    await ServiceUtils.teardown();
  });
});
