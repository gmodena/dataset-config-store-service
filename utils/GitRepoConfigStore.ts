import { ConfigStore, ConfigOptions } from "./ConfigStore";
import type { Storage, StorageValue } from "unstorage";
import fs from "node:fs";
import { parse } from "yaml";
import { execSync } from "child_process";
import path from "node:path";

const execCmd = (cmd: string, dir: string) => {
  console.log(`Executing command: ${cmd} in ${dir}`);
  execSync(cmd, {
    //   stdio: [0, 1, 2],
    stdio: "ignore",
    cwd: dir,
  });
};

export const updateConfigRepo = defineCachedFunction(
  (dir: string) => {
    console.log(`Repulling at ${dir}`);
    execCmd(`git checkout main && git pull`, dir);
    return dir;
  },
  {
    maxAge: useRuntimeConfig().cacheMaxAge,
    name: "configRepo",
    getKey: (dir: string) => dir,
    swr: false,
  }
);

export class GitRepoConfigStore extends ConfigStore {
  public gitDirectory: string;
  public configDevMode: boolean;
  constructor(
    storage?: Storage<StorageValue>,
    logger?: any,
    gitParentDirectory?: string,
    configDevMode?: boolean
  ) {
    super(storage);
    this.gitDirectory = path.resolve(
      gitParentDirectory ?? process.env.PWD,
      "config"
    );
    this.configDevMode = configDevMode;
    if (logger) {
      logger.log(
        "info",
        `GitRepoConfigStore instantiated with repo in ${this.gitDirectory} and configDevMode: ${configDevMode}`
      );
    }
  }

  getNormalizedConfig = async (
    path: string,
    options?: ConfigOptions
  ): Promise<object> => {
    let fileDir = this.gitDirectory;
    if (!this.configDevMode) {
      updateConfigRepo(this.gitDirectory);

      if (options.gitSHA) {
        if (await this.storage.hasItem(options.gitSHA)) {
          return await this.storage.getItem(options.gitSHA);
        }
        fileDir = `${this.gitDirectory}/${options.gitSHA}`;
        execCmd(
          `git worktree add ${fileDir} ${options.gitSHA}`,
          this.gitDirectory
        );
      }
    }

    const configYaml = fs.readFileSync(
      `${fileDir}/values/${path}/values.yaml`,
      "utf8"
    );
    const config = parse(configYaml);

    const schemaPath = config.$schema;
    if (!schemaPath) {
      throw new Error("Schema not found");
    }

    const configSchemaYaml = fs.readFileSync(
      `${fileDir}/jsonschema${schemaPath}.yaml`,
      "utf8"
    );
    const configSchema = parse(configSchemaYaml);

    const normalizaConfig = ConfigStore.normalizeConfig(configSchema, config);

    if (options.gitSHA && !this.configDevMode) {
      await this.storage.setItem(options.gitSHA, normalizaConfig);
      execCmd(`rm -rf ${fileDir}`, this.gitDirectory);
      execCmd(`git worktree prune`, this.gitDirectory);
    }
    return normalizaConfig;
  };
}
